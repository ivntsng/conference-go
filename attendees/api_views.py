from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Attendee
from events.models import Conference
import json
from common.json import ModelEncoder


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
    ]


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.

    {
        "attendees": [
            {
                "name": attendee's name,
                "href": URL to the attendee,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        response = []
        attendee = Attendee.objects.all()
        for people in attendee:
            response.append(
                {
                    "name": people.name,
                    "href": people.get_api_url(),
                }
            )
        return JsonResponse({"attendee": response})
    else:
        content = json.loads(request.body)
    try:
        conference = Conference.objects.get(id=conference_id)
        content["conference"] = conference
    except Conference.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid conference id"},
            status=400,
        )
    attendee = Attendee.objects.create(**content)
    return JsonResponse(
        attendee,
        encoder=AttendeeDetailEncoder,
        safe=False,
    )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, id):
    """
    Returns the details for the Attendee model specified
    by the id parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            {
                "email": attendee.email,
                "name": attendee.name,
                "company_name": attendee.company_name,
                "create": attendee.created,
                "conference": {
                    "name": attendee.conference.name,
                    "href": attendee.get_api_url(),
                },
            }
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
    try:
        if "conference" in content:
            conference = Conference.objects.get(
                conference=content["conference"]
            )
            content["conference"] = conference
    except Attendee.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid Conference Name"},
            status=400,
        )
    Attendee.objects.filter(id=id).update(**content)
    attendees = Attendee.objects.get(id=id)
    return JsonResponse(
        attendees,
        encoder=AttendeeDetailEncoder,
        safe=False
    )
